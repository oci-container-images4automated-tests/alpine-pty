Mix.install([
  {:buildah, git: "https://gitlab.com/oci-container-images4automated-tests/elixir-buildah.git", tag: "0.1.0-dev"},
  {:container_image_util, git: "https://gitlab.com/oci-container-images4automated-tests/container-image-util.git", tag: "0.1.0-dev"},
  {:oci_catatonit, git: "https://gitlab.com/oci-container-images4automated-tests/catatonit.git", tag: "0.1.0-dev"}
])

defmodule Buildah.Apk.Catatonit.Pty do

    alias Buildah.{Apk, Cmd}

    @catatonit_cmd Apk.Catatonit.catatonit_cmd()

    def on_container(container, options) do
        Apk.packages_no_cache(container, [
            "bash",
            "catatonit",
            "coreutils", # for printf, tty, stdbuf
            "util-linux" # util-linux-misc for script in edge
        ], options)
        {catatonit_exec_, 0} = Cmd.run(
            container,
            ["sh", "-c", "command -v " <> @catatonit_cmd]
        )
        IO.puts(String.trim(catatonit_exec_))
        # {_, 0} = Cmd.config(
            # container,
            # cmd: ""
        # )
        {_, 0} = Cmd.config(
            container,
            # entrypoint: "[\"/usr/bin/script\", \"--return\", \"--flush\", \"--command\", \"sh\", \"/dev/null\"]"
            #^ Working (worked rarely) with bash installed, blocking otherwise (with defaut busybox sh).
            #^ Does not work with catatonit!
            entrypoint: "[\"/bin/bash\", \"-c\", \"echo_printf() { echo \\\\\\\"$(/usr/bin/printf \\\"%q \\\" \\\"$@\\\")\\\\\\\"; } ; type echo_printf ; echo_printf \\\"${@}\\\" ; scripttty() { script --return --flush --command \\\"sh -c 'eval \\\\\\\"$(/usr/bin/printf \\\"%q \\\" \\\"$@\\\")\\\\\\\"'\\\" /dev/null ; } ; type scripttty ; scripttty tty ; scripttty \\\"${@}\\\"\", \"--\"]"
            # entrypoint: "[\"/bin/bash\", \"-c\", \"scripttty() { script --return --flush --command \\\"$(/usr/bin/printf \"%q \" \"$@\")\\\" /dev/null ; type scripttty ; scripttty tty ; scripttty \\\"${@}\\\"}\", \"--\"]"
            #^ not tryed, apparent {} problem

            # entrypoint: "[\"/bin/bash\", \"-c\", \"echo ${@:+--command $(printf \\\"%q \\\" \\\"${@}\\\")\\\"} ; exec script --return --flush ${@:+--command $(printf \\\"%q \\\" \\\"${@}\\\")\\\"} /dev/null\", \"--\"]"
            #^ Nothing

            # entrypoint: "[\"stdbuf\", \"-i0\", \"-o0\", \"-e0\", \"/bin/bash\", \"-c\", \"echo ${@:+--command \\\"stdbuf -i0 -o0 -e0 $(printf \\\"%q \\\" \\\"${@}\\\")\\\"} ; exec script --return --flush ${@:+--command \\\"stdbuf -i0 -o0 -e0 $(printf \\\"%q \\\" \\\"${@}\\\")\\\"} /dev/null\", \"--\"]"
            # May have worked?
            # Bug: level of \\\..."

            # entrypoint: "[\"/bin/sh\", \"-c\", \"echo ${@:+--command \\\"$(printf \\\"%q \\\" \\\"${@}\\\")\\\"} ; exec script --return --flush ${@:+--command \\\"$(printf \\\"%q \\\" \\\"${@}\\\")\\\"} /dev/null\", \"--\"]"
            #^ sh: %q : invalid format

            #entrypoint: "[\"/bin/sh\", \"-c\", \"echo ${@:+--command \\\\\\\"${@}\\\\\\\"} ; exec script --return --flush ${@:+--command \\\\\\\"${@}\\\\\\\"} /dev/null\", \"--\"]"
            #^ script: unrecognized option: x

            # entrypoint: "[\"/usr/bin/script\", \"--return\", \"--flush\", \"--command\", \"sh -c\", \"/dev/null\"]" # blocking

            # entrypoint: "[\"/bin/sh\"]" # /bin/sh: can't open 'sh': No such file or directory

            # entrypoint: "[\"/bin/sh\", \"-c\"]"

            # entrypoint: "[\"/bin/busybox\", \"sh\", \"-c\", \"exec /usr/bin/script --return --flush ${@:+--command \"${@}\"} /dev/null\", \"--\"]"

            # entrypoint: "[\"#{String.trim(catatonit_exec_)}\", \"--\", \"/bin/sh\", \"-c\", \"scriptf () { echo 8 \\\\\\\"${@}\\\\\\\" ; script --return --flush ${@:+--command \\\\\\\"sh -c \\\\\\\\\\\\\\\"${@}\\\\\\\\\\\\\\\"\\\\\\\"} /dev/null ; } ; type scriptf ; echo 4 \\\"$@\\\" ; echo 8 \\\\\\\"$@\\\\\\\" ; scriptf $@\", \"--\"]"

            # entrypoint: "[\"#{String.trim(catatonit_exec_)}\", \"--\", \"/bin/sh\", \"-c\", \"echo \\\"${@}\\\"; exec script --return --flush ${@:+--command \\\"${@}\\\"} /dev/null\", \"--\"]"
            # Bug: script command is not correctly escaped,but may work.
        )
        {inspected, 0} = Cmd.inspect(
            container)
        IO.puts(inspected)
    end
            # - '/bin/sh'
            # - '-c'
            # -  'exec runuser -u root --
                    # script
                        # --return
                        # --flush
                        # ${@:+--command "${@}"}
                        # /dev/null'
            # - '--'


    def test(container, image_ID, options) do
        {_, 0} = Cmd.run(container, [@catatonit_cmd, "--version"], into: IO.stream(:stdio, :line))
        # {_, 0} = Podman.Cmd.run(image_ID, [@catatonit_cmd, "--version"],
            #tty: true, rm: true, into: IO.stream(:stdio, :line)
        # )
        Apk.packages_no_cache(container, ["psmisc"], options)
        {_, 0} = Cmd.run(container, ["whoami"], into: IO.stream(:stdio, :line))
        {_, 0} = Cmd.run(container, ["pstree"], into: IO.stream(:stdio, :line))
        # {_, 0} = Podman.Cmd.run(image_ID, ["whoami"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # )
        # {_, 0} = Podman.Cmd.run(image_ID, ["pstree"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # )
    end

end


alias Buildah.Apk.{
    Catatonit,
    # Moreutils,
    # OpenJDK,
    # Php,
    # Sqlite,
    # Sudo
}

alias ContainerImageUtil

defmodule OciAlpinePty do

    def make_alpine_images(
            from_registry,
            from_name,
            to_registry,
            options \\ []
        ) do
        options = Keyword.merge([
                tag: nil #,
                # repositories_to_add: []
            ],
            options
        )
        tag = options[:tag]
        options = Keyword.merge([from_tag: tag], options)
        from_tag = options[:from_tag]
        # repositories_to_add = options[:repositories_to_add]
        # ci_registry_image = System.fetch_env!("CI_REGISTRY_IMAGE")
        # image_to_registry = System.fetch_env!("DESTINATION_REGISTRY")
        quiet = System.get_env("QUIET")

        # alpine/catatonit
        Buildah.from_push(
            ContainerImageUtil.image_name(
                from_registry, from_name, from_tag),
            &Catatonit.Pty.on_container/2,
            &Catatonit.Pty.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "catatonit", tag),
            quiet: quiet
        )

    end

end


alias Buildah.{Cmd, Print}

{whoami_, 0} = System.cmd("whoami", [])
"root" = String.trim(whoami_)
{_, 0} = Cmd.version(into: IO.stream(:stdio, :line))
{_, 0} = Cmd.info(into: IO.stream(:stdio, :line))

# from_name = System.fetch_env!("FROM_IMAGE")
# IO.puts(from_name)

# to_registry = System.fetch_env!("CI_REGISTRY_IMAGE")
# IO.puts(to_registry)

{parsed, _args, invalid} = OptionParser.parse(System.argv(), strict: [tag: :string])
IO.puts(invalid)

OciAlpinePty.make_alpine_images(
    "docker.io",
    "alpine",
    "docker://" <> System.fetch_env!("CI_REGISTRY_IMAGE"),
    parsed
)

Print.images()
